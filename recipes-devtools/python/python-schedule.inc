SUMMARY = "Job scheduling for humans."
HOMEPAGE = "https://github.com/dbader/schedule"
LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://LICENSE.txt;md5=6400f153491d45ea3459761627ca24b2"

inherit pypi

SRC_URI[md5sum] = "e29fd3b436b03220e147a4ed8191d220"
SRC_URI[sha256sum] = "f9fb5181283de4db6e701d476dd01b6a3dd81c38462a54991ddbb9d26db857c9"
