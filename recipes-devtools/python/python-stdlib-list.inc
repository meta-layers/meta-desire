SUMMARY = "A list of Python Standard Libraries."
HOMEPAGE = "https://github.com/jackmaney/python-stdlib-list"
LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://LICENSE;md5=8fe3d55921813400e0e295e623a5c2ae"

inherit pypi

SRC_URI[md5sum] = "a24d045958482553b9b17e40640ce169"
SRC_URI[sha256sum] = "5311a95812ebce3c5ad0b1f6ded798ce6f945d157075d166a5426c2da75a6625"


