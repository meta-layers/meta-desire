SUMMARY = "Python module dependency visualization."
HOMEPAGE = "https://github.com/thebjorn/pydeps"
LICENSE = "BSD"
LIC_FILES_CHKSUM = "file://${COMMON_LICENSE_DIR}/BSD;md5=3775480a712fc46a69647678acb234cb"

inherit pypi

SRC_URI[md5sum] = "a6209057e09832a532e440445b4fb891"
SRC_URI[sha256sum] = "3e46e9f855d470043b390ce0f763038bb8bbccc9168264fc5af77c202490fb33"

# we need this as a dependency
RDEPENDS_${PN} += "python3-stdlib-list"
# we need this for svg graphs ???
# RDEPENDS_${PN} += "graphviz"

