SUMMARY = "Client for interacting with InfluxDB"
HOMEPAGE = "https://github.com/influxdb/influxdb-python"
LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://LICENSE;md5=046523829184aac3703a4c60c0ae2104"

inherit pypi

SRC_URI[md5sum] = "01db77f4ca825d554a78804a4be4a353"
SRC_URI[sha256sum] = "30276c7e04bf7659424c733b239ba2f0804d7a1f3c59ec5dd3f88c56176c8d36"

# we need this as a dependency
RDEPENDS_${PN} += "python3-dateutil (>= 2.6.0)"
RDEPENDS_${PN} += "python3-pytz"
RDEPENDS_${PN} += "python3-requests (>= 2.17.0)"
RDEPENDS_${PN} += "python3-six (>= 1.10.0)"
RDEPENDS_${PN} += "python3-msgpack (>= 0.6.1)"
