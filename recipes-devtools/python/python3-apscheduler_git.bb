# Recipe created by recipetool
# This is the basis of a recipe and may need further editing in order to be fully functional.
# (Feel free to remove these comments when editing.)

# WARNING: the following LICENSE and LIC_FILES_CHKSUM values are best guesses - it is
# your responsibility to verify that the values are complete and correct.
#
# The following license files were not able to be identified and are
# represented as "Unknown" below, you will need to check them yourself:
#   LICENSE.txt
LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://LICENSE.txt;md5=f0e423eea5c91e7aa21bdb70184b3e53"

#SRC_URI = "git://github.com/agronholm/apscheduler;protocol=https"
SRC_URI = "git://github.com/agronholm/apscheduler;branch=3.x"

# Modify these as desired
PV = "3.6.3+git${SRCPV}"
SRCREV = "028506a816c74ee05951717c0e45d2e6ad32773e"
#SRCREV = "3b0d1ce3f3a607125e60cf87e0dc13f9f711cd5e"

S = "${WORKDIR}/git"

inherit setuptools3

DEPENDS += " python3-setuptools-scm-native"

# WARNING: the following rdepends are determined through basic analysis of the
# python sources, and might not be 100% accurate.
RDEPENDS_${PN} += "python3-asyncio python3-core python3-datetime python3-logging python3-math python3-netclient python3-pickle python3-pkg-resources python3-sqlalchemy"

RDEPENDS_${PN} += "python3-tzlocal python3-pytz"

# WARNING: We were unable to map the following python package/module
# dependencies to the bitbake packages which include them:
#    bson.binary
#    gevent.event
#    gevent.lock
#    kazoo.client
#    kazoo.exceptions
#    pymongo
#    pymongo.errors
#    pytz
#    pytz.exceptions
#    tornado.gen
#    tornado.ioloop
#    trollius
#    twisted.internet
#    tzlocal


