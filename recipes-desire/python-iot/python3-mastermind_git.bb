SUMMARY = "mastermind"
HOMEPAGE = "https://gitlab.com/robert.berger/python3-tcpclientserver"
LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://LICENSE;md5=8cd072e5c389ce416333b170bc423b66"

# only inherit sca if set in DISTRO_FEATURES
inherit ${@bb.utils.contains('DISTRO_FEATURES', 'sca', 'sca', '', d)}

#inherit python3-package-ident

SRC_URI = "${APP_URI_PREFIX}/recipes-desire/python-iot/python3-mastermind.git;protocol=https;branch=${APP_URI_BRANCH}"

# in case we have appbleeding defined in DISTRO_FEATURES it picks up SRCREV ?= ${AUTOREV}
require ${@ bb.utils.contains("DISTRO_FEATURES", "appbleeding", "conf/distro/include/appbleeding-floating.inc" , "", d)}

# in case we don't have appbleeding defined in DISTRO_FEATURES is pick up this SRCREV:
SRCREV ??= "caffebabe00000deadbeaf00000deba7ab1e0000"

# Modify these as desired
PV = "1.0.0+git${SRCPV}"

S = "${WORKDIR}/git"

inherit setuptools3

# WARNING: the following rdepends are from setuptools install_requires. These
# upstream names may not correspond exactly to bitbake package names.
RDEPENDS_${PN} += "python3-configparser"

# WARNING: the following rdepends are determined through basic analysis of the
# python sources, and might not be 100% accurate.
RDEPENDS_${PN} += "python3 python-core"

# other apps which depend on this:
#  python3-localdatacollector

