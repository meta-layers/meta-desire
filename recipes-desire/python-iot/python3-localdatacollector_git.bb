SUMMARY = "Local data collector"
HOMEPAGE = "https://gitlab.com/recipes-desire/python-iot/python3-localdatacollector"
LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://LICENSE;md5=8cd072e5c389ce416333b170bc423b66"

inherit python3-package-ident

# only inherit sca if set in DISTRO_FEATURES
inherit ${@bb.utils.contains('DISTRO_FEATURES', 'sca', 'sca', '', d)}

SRC_URI = "${APP_URI_PREFIX}/recipes-desire/python-iot/python3-localdatacollector.git;protocol=https;branch=${APP_URI_BRANCH}"

# in case we have appbleeding defined in DISTRO_FEATURES it picks up SRCREV ?= ${AUTOREV}
require ${@ bb.utils.contains("DISTRO_FEATURES", "appbleeding", "conf/distro/include/appbleeding-floating.inc" , "", d)}

# in case we don't have appbleeding defined in DISTRO_FEATURES is pick up this SRCREV:
SRCREV ??= "caffebabe00000deadbeaf00000deba7ab1e0000"

# Modify these as desired
PV = "2.8.3+git${SRCPV}"

S = "${WORKDIR}/git"

inherit setuptools3

# "standard" modules
DEPENDS += "python3-daemonize python3-schedule"
# our own modules
DEPENDS += "python3-messageqclient python3-tcpclientserver python3-mqttbrokerclient python3-mastermind"

###
# WARNING: python3-localdatacollector-2.8.3+gitAUTOINC+aadb427318-r0 do_package: python3-localdatacollector uses code from python-packages:python3-datetime,python3-schedule. Please add them to RDEPENDS_${PN}
###

RDEPENDS_${PN} += "python3-datetime python3-schedule"

RDEPENDS_${PN} += "python3-daemonize"

#RDEPENDS_${PN} += "python3 python-core python3-daemonize python3-schedule"

# looks like we need this as well
#RDEPENDS_${PN} += "python3-setuptools"

### 
#WARNING: python3-localdatacollector-2.8.3+gitAUTOINC+aadb427318-r0 do_package: python3-localdatacollector uses code from python-packages:python3-datetime. Please add them to RDEPENDS_${PN}
#WARNING: python3-localdatacollector-2.8.3+gitAUTOINC+aadb427318-r0 do_package: python3-localdatacollector don't use code from python-packages:python3-daemonize,python3-python3,python3-setuptools. Please remove them to RDEPENDS_${PN}
###


# This is our custom module
# install_requires versions in setup.py are handled by Python in runtime only!
# ideally this corresponds to the version (= x.x.x) here
RDEPENDS_${PN} += "python3-messageqclient (>= 1.0.0)"
RDEPENDS_${PN} += "python3-tcpclientserver (>= 1.0.0)"
RDEPENDS_${PN} += "python3-mqttbrokerclient (>= 2.8.4)"
RDEPENDS_${PN} += "python3-mastermind (>= 1.0.0)"


do_install_append() {
    install -d ${D}/${sysconfdir}/datacollector
    install -m 0644 ${S}/datacollector.conf ${D}/${sysconfdir}/datacollector/datacollector.conf
#    export PREFIX=${D}/${prefix}
#    oe_runmake install
#   install -d ${D}/${sysconfdir}/datacollector
#   install -m 0644 ${WORKDIR}/datacollector.conf ${D}/${sysconfdir}/datacollector/datacollector.conf
#    install -d ${D}/${sysconfdir}/init.d
#    install -m 0755 ${WORKDIR}/init-redis-server ${D}/${sysconfdir}/init.d/redis-server
#    install -d ${D}/var/lib/redis/
#    chown redis.redis ${D}/var/lib/redis/

#    install -d ${D}${systemd_system_unitdir}
#    install -m 0644 ${WORKDIR}/redis.service ${D}${systemd_system_unitdir}
#    sed -i 's!/usr/sbin/!${sbindir}/!g' ${D}${systemd_system_unitdir}/redis.service

#    if [ "${REDIS_ON_SYSTEMD}" = true ]; then
#        sed -i 's!daemonize yes!# daemonize yes!' ${D}/${sysconfdir}/redis/redis.conf
#    fi
}


