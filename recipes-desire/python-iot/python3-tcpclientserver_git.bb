SUMMARY = "Python3 module to open tcp-socket server for remote requests"
HOMEPAGE = "https://gitlab.com/robert.berger/python3-tcpclientserver"
LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://LICENSE;md5=448ce91f702f3cf5ab5fad4e861598aa"

inherit python3-package-ident

# only inherit sca if set in DISTRO_FEATURES
inherit ${@bb.utils.contains('DISTRO_FEATURES', 'sca', 'sca', '', d)}

SRC_URI = "${APP_URI_PREFIX}/recipes-desire/python-iot/python3-tcpclientserver.git;protocol=https;branch=${APP_URI_BRANCH}"
#SRC_URI = "git:///tmp/python3-tcpclientserver.git;protocol=file;branch=${APP_URI_BRANCH}"

# in case we have appbleeding defined in DISTRO_FEATURES it picks up SRCREV ?= ${AUTOREV}
require ${@ bb.utils.contains("DISTRO_FEATURES", "appbleeding", "conf/distro/include/appbleeding-floating.inc" , "", d)}

# in case we don't have appbleeding defined in DISTRO_FEATURES is pick up this SRCREV:
SRCREV ??= "caffebabe00000deadbeaf00000deba7ab1e0000"

# Modify these as desired
PV = "1.0.0+git${SRCPV}"

S = "${WORKDIR}/git"

inherit setuptools3

# to make sca happy
DEPENDS += "python3-nmapwrapper"

#### warrior:
#WARNING: python3-tcpclientserver-1.0.0+gitAUTOINC+faf7904667-r0 do_package: python3-tcpclientserver uses code from python-packages:python3-io. Please add them to RDEPENDS_${PN}
#WARNING: python3-tcpclientserver-1.0.0+gitAUTOINC+faf7904667-r0 do_package: python3-tcpclientserver don't use code from python-packages:python3-configparser,python3-python3. Please remove them to RDEPENDS_${PN}
####

#### zeus:
#WARNING: python3-tcpclientserver-1.0.0+gitAUTOINC+2c8a379788-r0 do_package: python3-tcpclientserver uses code from python-packages:python3-pickle. Please add them to RDEPENDS_${PN}
#WARNING: python3-tcpclientserver-1.0.0+gitAUTOINC+2c8a379788-r0 do_package: python3-tcpclientserver don't uses code from python-packages:python3-configparser,python3-nmapwrapper,python3-setuptools. Please remove them from RDEPENDS_${PN}
####

RDEPENDS_${PN} += "python3-io python3-core"
#RDEPENDS_${PN} += "python3 python-core python-datetime python3-io python-io python-logging python-pickle python-subprocess"

RDEPENDS_${PN} += "python3-setuptools python3-configparser"

# WARNING: the following rdepends are from setuptools install_requires. These
# upstream names may not correspond exactly to bitbake package names.
#RDEPENDS_${PN} += "python3-configparser"

# WARNING: the following rdepends are determined through basic analysis of the
# python sources, and might not be 100% accurate.
#RDEPENDS_${PN} += "python3 python-core"

# This is our custom module
# install_requires versions in setup.py are handled by Python in runtime only!
# ideally this corresponds to the version (= x.x.x) here
RDEPENDS_${PN} += "python3-nmapwrapper (>= 2.8.3)"

# other apps which depend on this:
#  python3-localdatacollector

do_install_append() {
    install -d ${D}/${sysconfdir}/tcpclientserver
    install -m 0644 ${S}/tcpclientserver.conf ${D}/${sysconfdir}/tcpclientserver/tcpclientserver.conf
}

# package splitting
# we have here 1 entry point and one config file 
# which in only valid for the server
# if we use it as a client it's just like a lib/module

PACKAGES =+ "${PN}-server"

# server

FILES_${PN}-server += " \
     ${bindir}/tcpclientserver-server \
     ${sysconfdir}/tcpclientserver/tcpclientserver.conf \
"

#RDEPENDS_${PN}-server += "${PN}"
RDEPENDS_${PN}-server += "python3-core"
