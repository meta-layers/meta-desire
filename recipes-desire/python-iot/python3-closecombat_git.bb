# Recipe created by recipetool
# This is the basis of a recipe and may need further editing in order to be fully functional.
# (Feel free to remove these comments when editing.)

SUMMARY = "Python3 module to collect and display configuration data from INI files"
HOMEPAGE = "https://gitlab.com/robert.berger/close-combat"
# WARNING: the following LICENSE and LIC_FILES_CHKSUM values are best guesses - it is
# your responsibility to verify that the values are complete and correct.
LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://LICENSE;md5=0835ade698e0bcf8506ecda2f7b4f302"

SRC_URI = "git://gitlab.com/robert.berger/close-combat.git;protocol=https"

# Modify these as desired
PV = "1.0.0+git${SRCPV}"
SRCREV = "cc90b8534eed1170e7f6922122a8fba8def297eb"

S = "${WORKDIR}/git"

inherit setuptools3

# WARNING: the following rdepends are from setuptools install_requires. These
# upstream names may not correspond exactly to bitbake package names.
RDEPENDS_${PN} += "python3-configparser"

# WARNING: the following rdepends are determined through basic analysis of the
# python sources, and might not be 100% accurate.
#RDEPENDS_${PN} += "python3-core"

# looks like we need this as well
RDEPENDS_${PN} += "python3-setuptools"

# ntpath is somewhere in here
RDEPENDS_${PN} += "python3"
