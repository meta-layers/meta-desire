SUMMARY = "Mqtt broker client"
HOMEPAGE = "https://gitlab.com/recipes-desire/python-iot/python3-mqttbrokerclient"
LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://LICENSE;md5=8cd072e5c389ce416333b170bc423b66"

# only inherit sca if set in DISTRO_FEATURES
inherit ${@bb.utils.contains('DISTRO_FEATURES', 'sca', 'sca', '', d)}

SRC_URI = "${APP_URI_PREFIX}/recipes-desire/python-iot/python3-mqttbrokerclient.git;protocol=https;branch=${APP_URI_BRANCH}"

# in case we have appbleeding defined in DISTRO_FEATURES it picks up SRCREV ?= ${AUTOREV}
require ${@ bb.utils.contains("DISTRO_FEATURES", "appbleeding", "conf/distro/include/appbleeding-floating.inc" , "", d)}

# in case we don't have appbleeding defined in DISTRO_FEATURES is pick up this SRCREV:
SRCREV ??= "caffebabe00000deadbeaf00000deba7ab1e0000"

# Modify these as desired
PV = "2.8.4+git${SRCPV}"

S = "${WORKDIR}/git"

inherit setuptools3

DEPENDS += "python3-paho-mqtt"

DEPENDS += "python3-sqlalchemy python3-apscheduler python3-psutil python3-redis"

DEPENDS += "python3-influxdb"

# WARNING: the following rdepends are determined through basic analysis of the
# python sources, and might not be 100% accurate.
#RDEPENDS_${PN} += "python3 python-core python-logging"

# WARNING: We were unable to map the following python package/module
# dependencies to the bitbake packages which include them:
#    messageqclient.redis.simple_set
#    messageqclient.redis.simple_test
#    redis

RDEPENDS_${PN} += "python3"

# to read the config file
RDEPENDS_${PN} += "python3-configparser"

# looks like we need this as well
RDEPENDS_${PN} += "python3-setuptools"

# This is a standard python module
# install_requires versions in setup.py are handled by Python in runtime only!
# ideally this corresponds to the version (= x.x.x) here
RDEPENDS_${PN} += "python3-paho-mqtt (>= 1.4.0)"

RDEPENDS_${PN} += "python3-sqlalchemy (>= 1.1.17)"

RDEPENDS_${PN} += "python3-apscheduler (>= 3.6.3)"

RDEPENDS_${PN} += "python3-psutil (>= 5.6.3)"

RDEPENDS_${PN} += "python3-redis (>= 2.10.6)"

RDEPENDS_${PN} += "python3-influxdb (>= 5.2.3)"

# from 25 to 39 Megs - I hope we really need it ;)
RDEPENDS_${PN} += "python3-pymysql (>= 0.9.3)"

# other apps which depend on this:
#  python3-localdatacollector

# package splitting
# we have here 1 entry point and one config file
# which in only valid for the executor
# if we use it as a client it's just like a lib/module

PACKAGES =+ "${PN}-executor"

# executor

#FILES_${PN}-executor += " \
#     ${bindir}/mqttbrokerclient-executor \
#     ${sysconfdir}/mqttbrokerclient-executor/mqttbrokerclient-executor.conf \
#"

FILES_${PN}-executor += " \
     ${bindir}/mqttbrokerclient-executor \
"


