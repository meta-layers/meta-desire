SUMMARY = "Wrapper of NMAP native Linux command...Listens to remote requests for local network"
HOMEPAGE = "https://gitlab.com:robert.berger/python3-nmapwrapper"
LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://LICENSE;md5=448ce91f702f3cf5ab5fad4e861598aa"

inherit python3-package-ident

# only inherit sca if set in DISTRO_FEATURES
inherit ${@bb.utils.contains('DISTRO_FEATURES', 'sca', 'sca', '', d)}

SRC_URI = "${APP_URI_PREFIX}/recipes-desire/python-iot/python3-nmapwrapper.git;protocol=https;branch=${APP_URI_BRANCH}"

# in case we have appbleeding defined in DISTRO_FEATURES it picks up SRCREV ?= ${AUTOREV}
require ${@ bb.utils.contains("DISTRO_FEATURES", "appbleeding", "conf/distro/include/appbleeding-floating.inc" , "", d)}

# in case we don't have appbleeding defined in DISTRO_FEATURES is pick up this SRCREV:
SRCREV ??= "caffebabe00000deadbeaf00000deba7ab1e0000"

# Modify these as desired
PV = "2.8.3+git${SRCPV}"

S = "${WORKDIR}/git"

inherit setuptools3

DEPENDS += "python3-daemonize"

# WARNING: the following rdepends are determined through basic analysis of the
# python sources, and might not be 100% accurate.
#RDEPENDS_${PN} += "python3 python-core python-datetime python-io python-logging python-pickle python-subprocess"

# looks like we need this as well
RDEPENDS_${PN} += "python3-setuptools python3-daemonize"

RDEPENDS_${PN} += "python3-datetime"

#### zeus:
#WARNING: python3-nmapwrapper-2.8.3+gitAUTOINC+464eace731-r0 do_package: python3-nmapwrapper don't uses code from python-packages:python3-daemonize,python3-setuptools. Please remove them from RDEPENDS_${PN}

#### warrior:
#WARNING: python3-nmapwrapper-2.8.3+gitAUTOINC+bf1396d0bf-r0 do_package: python3-nmapwrapper uses code from python-packages:python3-datetime. Please add them to RDEPENDS_${PN}
#WARNING: python3-nmapwrapper-2.8.3+gitAUTOINC+bf1396d0bf-r0 do_package: python3-nmapwrapper don't use code from python-packages:python3-daemonize,python3-python3,python3-setuptools. Please remove them to RDEPENDS_${PN}
####
