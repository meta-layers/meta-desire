SUMMARY = "MessageQ client"
HOMEPAGE = "https://gitlab.com/recipes-desire/python-iot/python3-messageqclient"
LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://LICENSE;md5=448ce91f702f3cf5ab5fad4e861598aa"

# only inherit sca if set in DISTRO_FEATURES
inherit ${@bb.utils.contains('DISTRO_FEATURES', 'sca', 'sca', '', d)}

SRC_URI = "${APP_URI_PREFIX}/recipes-desire/python-iot/python3-messageqclient.git;protocol=https;branch=${APP_URI_BRANCH}"

# in case we have appbleeding defined in DISTRO_FEATURES it picks up SRCREV ?= ${AUTOREV}
require ${@ bb.utils.contains("DISTRO_FEATURES", "appbleeding", "conf/distro/include/appbleeding-floating.inc" , "", d)}

# in case we don't have appbleeding defined in DISTRO_FEATURES is pick up this SRCREV:
SRCREV ??= "caffebabe00000deadbeaf00000deba7ab1e0000"

# Modify these as desired
PV = "1.0.0+git${SRCPV}"

S = "${WORKDIR}/git"

inherit setuptools3

# "standard" modules
DEPENDS += "python3-redis"

# WARNING: the following rdepends are determined through basic analysis of the
# python sources, and might not be 100% accurate.
#RDEPENDS_${PN} += "python3 python-core python-logging"

# WARNING: We were unable to map the following python package/module
# dependencies to the bitbake packages which include them:
#    messageqclient.redis.simple_set
#    messageqclient.redis.simple_test
#    redis

RDEPENDS_${PN} += "python3"

# to read the config file
RDEPENDS_${PN} += "python3-configparser"

# looks like we need this as well
RDEPENDS_${PN} += "python3-setuptools"

# This is a standard python module
# install_requires versions in setup.py are handled by Python in runtime only!
# ideally this corresponds to the version (= x.x.x) here
RDEPENDS_${PN} += "python3-redis (>= 2.10.6)"

# other apps which depend on this:
#  python3-localdatacollector
