SUMMARY = "Python3 module scanning new messages in mqtt-brokers and sending emails in case of important events"
HOMEPAGE = "https://gitlab.com:robert.berger/python3-carbonmessage"
LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://LICENSE;md5=3e96c57e45d1e5ff2617912f3d7cd7ce"

# only inherit sca if set in DISTRO_FEATURES
inherit ${@bb.utils.contains('DISTRO_FEATURES', 'sca', 'sca', '', d)}

#inherit python-package-ident

SRC_URI = "${APP_URI_PREFIX}/recipes-desire/python-iot/python3-carbonmessage.git;protocol=https;branch=${APP_URI_BRANCH}"

# in case we have appbleeding defined in DISTRO_FEATURES it picks up SRCREV ?= ${AUTOREV}
require ${@ bb.utils.contains("DISTRO_FEATURES", "appbleeding", "conf/distro/include/appbleeding-floating.inc" , "", d)}

# in case we don't have appbleeding defined in DISTRO_FEATURES is pick up this SRCREV:
SRCREV ??= "caffebabe00000deadbeaf00000deba7ab1e0000"

# Modify these as desired
PV = "1.0.0+git${SRCPV}"

S = "${WORKDIR}/git"

inherit setuptools3

DEPENDS += "python3-paho-mqtt"

# Python "standard" modules
RDEPENDS_${PN} += "python3"
RDEPENDS_${PN} += "python3-setuptools"
RDEPENDS_${PN} += "python3-paho-mqtt (>= 1.4.0)"
RDEPENDS_${PN} += "python3-daemonize"


# same config file for subscriber and publisher as requested by miltos
#do_install_append() {
#    install -d ${D}/${sysconfdir}/carbonmessage
#    install -m 0644 ${S}/carbonmessage.conf ${D}/${sysconfdir}/carbonmessage/carbonmessage.conf
#    export PREFIX=${D}/${prefix}
#    oe_runmake install
#   install -d ${D}/${sysconfdir}/datacollector
#   install -m 0644 ${WORKDIR}/datacollector.conf ${D}/${sysconfdir}/datacollector/datacollector.conf
#    install -d ${D}/${sysconfdir}/init.d
#    install -m 0755 ${WORKDIR}/init-redis-server ${D}/${sysconfdir}/init.d/redis-server
#    install -d ${D}/var/lib/redis/
#    chown redis.redis ${D}/var/lib/redis/

#    install -d ${D}${systemd_system_unitdir}
#    install -m 0644 ${WORKDIR}/redis.service ${D}${systemd_system_unitdir}
#    sed -i 's!/usr/sbin/!${sbindir}/!g' ${D}${systemd_system_unitdir}/redis.service

#    if [ "${REDIS_ON_SYSTEMD}" = true ]; then
#        sed -i 's!daemonize yes!# daemonize yes!' ${D}/${sysconfdir}/redis/redis.conf
#    fi
#}


# package splitting
# we have here 2 entry points/apps, but need only 
# .one per conainter/image so we split them out

#PACKAGES =+ "${PN}-publisher ${PN}-subscriber"

# publisher

#FILES_${PN}-publisher += " \ 
#    ${bindir}/carbonmessage-publisher \
#"

#RDEPENDS_${PN}-publisher += "${PN}"

# subscriber

#FILES_${PN}-subscriber += " \
#    ${bindir}/carbonmessage-subscriber \
#"

#RDEPENDS_${PN}-subscriber += "${PN}"



