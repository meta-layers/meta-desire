# is included from each of the recipes (_pn) here - if DISTRO_FEATURES contains appbleeding

SRCREV_pn-python3-carbonmessage ?= "${AUTOREV}"
SRCREV_pn-python3-localdatacollector ?= "${AUTOREV}"
SRCREV_pn-python3-mastermind ?= "${AUTOREV}"
SRCREV_pn-python3-messageqclient ?= "${AUTOREV}"
SRCREV_pn-python3-mqttbrokerclient ?= "${AUTOREV}"
SRCREV_pn-python3-nmapwrapper ?= "${AUTOREV}"
SRCREV_pn-python3-tcpclientserver ?= "${AUTOREV}"
