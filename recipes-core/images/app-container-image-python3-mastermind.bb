# Copyright (C) 2020 Robert Berger <robert.berger@ReliableEmbeddedSystems.com>
# Released under the MIT license (see COPYING.MIT for the terms)

SUMMARY = "A mastermind image"

# only inherit sca if set in DISTRO_FEATURES
inherit ${@bb.utils.contains('DISTRO_FEATURES', 'sca', 'sca', '', d)}

# in case we have scaforce defined in DISTRO_FEATURES include a file, which enforces compile -> sca
require ${@ bb.utils.contains("DISTRO_FEATURES", "scaforce", "conf/distro/include/sca-force.inc" , "", d)}

require recipes-core/images/app-container-image.bb
require recipes-core/images/app-container-image-python3-mastermind-common.inc
