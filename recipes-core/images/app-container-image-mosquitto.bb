# Copyright (C) 2020 Robert Berger <robert.berger@ReliableEmbeddedSystems.com>
# Released under the MIT license (see COPYING.MIT for the terms)

SUMMARY = "A mosquitto image"

require recipes-core/images/app-container-image.bb
require recipes-core/images/app-container-image-mosquitto-common.inc
