# Note that busybox is required to satify /bin/sh requirement of lghttpd,
# and the access* modules need to be explicitly specified since RECOMMENDATIONS
# are disabled.
IMAGE_INSTALL += " \
	busybox \
        python3 \
        python3-mqttbrokerclient \
	python3-mqttbrokerclient-executor \
"

# For testing
#IMAGE_INSTALL += " \
#        python3-pydeps \
#"
