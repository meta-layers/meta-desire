# Note that busybox is required to satify /bin/sh requirement of lghttpd,
# and the access* modules need to be explicitly specified since RECOMMENDATIONS
# are disabled.
IMAGE_INSTALL += " \
	busybox \
	nmap \
        python3 \
        python3-tcpclientserver \
        python3-tcpclientserver-server \
"

# For testing
#IMAGE_INSTALL += " \
#        python3-pydeps \
#"

